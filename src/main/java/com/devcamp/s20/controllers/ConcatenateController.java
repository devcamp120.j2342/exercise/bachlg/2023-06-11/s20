package com.devcamp.s20.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ConcatenateController {
    @GetMapping("/concatenate")
    public String concatenateStrings(@RequestParam("str1") String str1, @RequestParam("str2") String str2) {
        int minLength = Math.min(str1.length(), str2.length());
        String newStr1 = str1.substring(str1.length() - minLength);
        String newStr2 = str2.substring(str2.length() - minLength);
        return newStr1 + newStr2;
    }
}
